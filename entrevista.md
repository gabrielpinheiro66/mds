### Meu "pitch"

- Entrei na UNIFEI e logo de cara na Black Bee. Fui para a área de hardware. Após treinamentos me interessei pela parte de integração do software no drone. Assim sempre estive perto das duas áreas

- Explicar a BlackBee (Drones autônomos e talz)

- Coisas que fiz lá além das coisas técnicas: Apresentações, workshops, organização da Formula Drone, dei cursos, participei de eventos (exposição FINIT e DroneShow), participei do IMAV 2019, tive paper publicado no IMAV 2018

- Coisas técnicas: integração com a parte de software, montagem e configuração de drones; programação de dispositivos com linux embarcado; ministério de cursos pela equipe; gerência da área de hardware. Na parte de integração com o software trabalhei com MAVROS (ROS para MAV's), Tensorflow, OpenCV, etc

- Após isso entrei na Asimov (falar um pouco sobre)

- Desenvolvimento de projetos de automação com linux embarcado; Desenvolvimento web principalmente com Angular e frameworks Python (FastAPI, Django, Flask); Participação em reuniões com clientes e possíveis clientes, etc.

- Estágio na Sisvoo: basicamente resolvia problemas relacionados ao servidor da empresa, à rede e aos computadores. **Fiz scripts de automação de tarefas, algo q a Smarthis trabalha**

---

### Perguntas aleatórias

- **Qual minha trajetória??** PITCH

- **Porque eu escolhi a SMARTHIS??** Me interessei bastante pelo que a empresa faz. Nas três áreas. No último estágio fiz um programa para automatizar uma tarefa (contar história).

- **Porque SMARTHIS (cultura)??**
	- Fato de ser home office e flexível (dizer pq com relaçao ao ultimo estag);
	- É uma empresa apaixonada por tecnologia;
	- Vi que é uma startup muito reconhecida e que deve crescer muito (assim dá oportunidade de crescimanto);
	- Me identifiquei bastante com a missão da empresa; 
	- Buscam pessoas que respeitem essas diferenças e abracem a diversidade; 

- **Que coisas te definem como pessoa?** Sou sério, exigente, fácil convivência, quieto, 

- **Que coisas você não gosta? (no trabalho)** Não conhecer o processo ao qual estou inserido, 

- **Que coisas você gosta?  (no trabalho)**  Gosto de interlocução/feedback,   

- **Quais são as suas qualidades?** Muito auto-didata, hábito de ler documentações,

- **Quais são os seus defeitos?** TENHO DIFICULDADE PRA ADMITIR MEUS DEFEITOS

- **Que coisas você gosta de fazer?** Gosto muito de tecnologia, futebol, metal, gostava de jogar pôquer mas nao tenho mais tempo. Gosto de séries e filmes, principalmente de suspense e terror

---

### Perguntas técnicas

- **Quais linguagens de programação?** Python, Node/JS, C++, C, Rust, Bash/Shell

- **Projetos?** Bots para jogar tribal, Bot para jogar (esqueci) anti captcha, Bot para zap, Programa p/ copiar, [....]

- **Matérias q mais gostou?** POO, Banco de dados, Circuitos II e Polifásicos, Automação e Supervisórios 
 
- **Matérias q menos gostou?** Desenhos, Processos de fabricação, Mecsol, Materiais


---

### Outras coisas

- **Missão:** Acreditamos que boas soluções surgem de fortes conexões entre pessoas, conhecimentos, ideias e ideais. Por isso, ser Smarther é mais do que fazer parte de uma empresa, é viver verdadeiramente a cultura da colaboração e inovação que pulsa em nosso DNA.
